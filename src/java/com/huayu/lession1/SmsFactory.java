package com.huayu.lession1;

public class SmsFactory implements Provider{

    public Sender init() {
        return new SmsSender();
    }
}
