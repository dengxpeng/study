package com.huayu.lession2;

public class SingleTon {

    private SingleTon(){

    }

    private static class SingleTonBuilder{
        private static SingleTon singleTon = new SingleTon();
    }

    public static SingleTon getInstance(){
        return SingleTonBuilder.singleTon;
    }


}
