package com.huayu.io.nio;

import jdk.nashorn.internal.ir.IdentNode;

import java.nio.IntBuffer;

public class TestBuffer {

    public static void main(String[] args) {
        IntBuffer intBuffer = IntBuffer.allocate(10);
        intBuffer.put(3);
        intBuffer.put(4);
        intBuffer.put(5);
        System.out.printf("位置：%d,长度：%d,capacity:%d\n",intBuffer.position(),intBuffer.limit(),intBuffer.capacity());
        intBuffer.flip();
        System.out.printf("位置：%d,长度：%d,capacity:%d",intBuffer.position(),intBuffer.limit(),intBuffer.capacity());

    }
}
