package com.huayu.lession3;

public class Wrapper implements Target{
    private Source source;

    public Wrapper(Source source) {
        this.source = source;
    }

    public void m1() {
        source.m1();
    }

    public void m2() {
        System.out.println("这是对象适配器。");
    }
}
