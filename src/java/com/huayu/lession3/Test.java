package com.huayu.lession3;

public class Test {

    public static void main(String[] args) {
//        Target target = new Adapter();
//        target.m1();
//        target.m2();
        Source source = new Source();
        Target target = new Wrapper(source);
        target.m1();
        target.m2();
    }
}
