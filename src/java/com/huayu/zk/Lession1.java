package com.huayu.zk;

import org.apache.zookeeper.*;

import java.io.IOException;

public class Lession1 {

    public static void main(String[] args) throws IOException, InterruptedException, KeeperException {
        ZooKeeper zooKeeper = new ZooKeeper("localhost:2181", 1000, watchedEvent -> {
            System.out.println("建立连接");
        });
        zooKeeper.create("/dengpeng2","测试".getBytes(), ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);
        zooKeeper.close();

    }
}
