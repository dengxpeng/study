package com.huayu.threadpools.lession1;

import java.util.concurrent.*;

public class TestCR {

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        Callable01 callable01 = new Callable01();

        ExecutorService executorService = Executors.newCachedThreadPool();
        Future<String> submit = executorService.submit(callable01);

        System.out.println(submit.get());
        executorService.submit(new Runnable01());
        executorService.shutdown();



    }

    static class Callable01 implements Callable<String>{

        @Override
        public String call() throws Exception {
            return "Callable返回。。。哈喽!";
        }
    }

    static class Runnable01 implements Runnable{

        @Override
        public void run() {
            System.out.println("Runnable打印。。。。");
        }
    }


}
