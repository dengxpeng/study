package com.huayu.io.aio;

import java.net.InetSocketAddress;
import java.nio.channels.AsynchronousChannelGroup;
import java.nio.channels.AsynchronousServerSocketChannel;
import java.util.PrimitiveIterator;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Server {

    private ExecutorService executorService;

    private AsynchronousChannelGroup channelGroup;

    public AsynchronousServerSocketChannel assc;

    public Server(int port) {
        try{
            executorService = Executors.newCachedThreadPool();
            channelGroup = AsynchronousChannelGroup.withCachedThreadPool(executorService,1);
            assc = AsynchronousServerSocketChannel.open(channelGroup);
            assc.bind(new InetSocketAddress(port));
            System.out.println("port:"+port);
            assc.accept(this,new ServerCompletionHandler());
            Thread.sleep(Integer.MAX_VALUE);

        }catch (Exception e){

        }

    }

    public static void main(String[] args) {
        Server server = new Server(8765);

    }
}
