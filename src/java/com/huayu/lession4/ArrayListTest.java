package com.huayu.lession4;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ArrayListTest {

    public static void main(String[] args) throws InterruptedException {
//        List list = new ArrayList<String>();
        List list = new CopyOnWriteArrayList();
        ExecutorService executorService = Executors.newCachedThreadPool();
        executorService.submit(()->{
            for (int i = 0; i < 10000; i++) {
                list.add(i);
            }
        });
        executorService.submit(()->{
            for (int i = 0; i < 10000; i++) {
                list.add(i);
            }
        });
        executorService.shutdown();
//        for (int i = 0; i < 2000; i++) {
//            System.out.println(list.get(i));;
//        }
        Thread.sleep(2000);
        System.out.println(list.size());

    }

}
