package com.huayu.threadpools.lession1;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Thread01 {

    public static void main(String[] args) {
        ExecutorService executorService = Executors.newCachedThreadPool();
        executorService.submit(()->{

            System.out.println("你好");
        });
        executorService.shutdown();
    }

}
