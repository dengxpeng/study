package com.huayu.lession1;

public class Test {

    public static void main(String[] args) {
        Provider provider = new SmsFactory();
        Sender init = provider.init();
        init.send();
    }
}
