package com.huayu.lession1;

public class MailFactory implements Provider{

    public Sender init() {
        return new MailSender();
    }
}
